package com.xxjshenxx.practica_1_t3_n00022769;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.xxjshenxx.practica_1_t3_n00022769.Adapter.NameAdapter;

import java.util.Arrays;
import java.util.List;

public class AgendaActivity extends AppCompatActivity {

    private String Victor = "Victor";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        RecyclerView rv = findViewById(R.id.rv_Agenda);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        List<String> L_NO = Arrays.asList("MARLO","JESSICA","WILSON","LUCERO","CRISTIAN","PROFE","MAMÁ","PAPÁ","ANDER","MARIA");
        List<String> L_NU = Arrays.asList("969 052 261","965 261 470","968 922 313","962 524 650","966 588 204","962 197 993","969 503 731","968 345 941","960 033 081","960 009 037");
        NameAdapter adapter = new NameAdapter(L_NO,L_NU);
        rv.setAdapter(adapter);


    }
}