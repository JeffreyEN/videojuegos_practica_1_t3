package com.xxjshenxx.practica_1_t3_n00022769.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.xxjshenxx.practica_1_t3_n00022769.R;

import java.util.List;

public class NameAdapter extends RecyclerView.Adapter<NameAdapter.NameViewHolder> {

    Context context;
    private List<String> nom;
    private List<String> num;



    public NameAdapter(List<String> L_NO, List<String> L_NU) {
        this.nom = L_NO;
        this.num = L_NU;
        this.context = context;
    }



    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_name, parent, false);
        return new NameViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NameAdapter.NameViewHolder holder, int i) {
        TextView tv_nom = holder.itemView.findViewById(R.id.tv_contactos);
        String value_no = nom.get(i);
        tv_nom.setText(value_no);

        TextView tv_num = holder.itemView.findViewById(R.id.tv_numeros);
        String value_nu = num.get(i);
        tv_num.setText(value_nu);
        Button button_Call = holder.itemView.findViewById(R.id.bt_llamadas);
        button_Call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Toast.makeText(holder.itemView.getContext(), value_no, Toast.LENGTH_SHORT).show();
                Toast.makeText(holder.itemView.getContext(), value_nu, Toast.LENGTH_SHORT).show();*/
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",value_nu,null));
                holder.itemView.getContext().startActivity(intent);
            }
        });
        Button button_Sms = holder.itemView.findViewById(R.id.bt_mensajes);
        button_Sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Toast.makeText(holder.itemView.getContext(), value_no, Toast.LENGTH_SHORT).show();
                Toast.makeText(holder.itemView.getContext(), value_nu, Toast.LENGTH_SHORT).show();*/
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms",value_nu,null));
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return num.size();
    }

    class NameViewHolder extends RecyclerView.ViewHolder {

        public NameViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}


